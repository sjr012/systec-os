package com.systecos.boot.dao;

import org.springframework.stereotype.Repository;

import com.systecos.boot.domain.Departamento;

@Repository
public class DepartamentoDaoImpl extends AbstractDao <Departamento, Long> implements DepartamentoDao {

}
