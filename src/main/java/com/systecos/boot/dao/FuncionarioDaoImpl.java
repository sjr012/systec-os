package com.systecos.boot.dao;

import org.springframework.stereotype.Repository;

import com.systecos.boot.domain.Funcionario;

@Repository
public class FuncionarioDaoImpl extends AbstractDao<Funcionario, Long> implements FuncionarioDao {

}
