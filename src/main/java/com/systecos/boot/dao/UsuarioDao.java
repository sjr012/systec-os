package com.systecos.boot.dao;

import java.util.List;

import com.systecos.boot.domain.Usuario;

public interface UsuarioDao {

	void save (Usuario usuario);
	
	void update (Usuario usuario);
	
	void delete (Long id);
	
	Usuario findById (Long id);
	
	List <Usuario> findAll();
	
}
