package com.systecos.boot.dao;

import org.springframework.stereotype.Repository;

import com.systecos.boot.domain.Usuario;

@Repository
public class UsuarioDaoImpl extends AbstractDao <Usuario, Long> implements UsuarioDao {

}
