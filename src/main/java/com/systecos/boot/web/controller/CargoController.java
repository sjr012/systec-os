package com.systecos.boot.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.systecos.boot.domain.Cargo;
import com.systecos.boot.domain.Departamento;
import com.systecos.boot.service.CargoService;
import com.systecos.boot.service.DepartamentoService;

@Controller
@RequestMapping ("/cargos")
public class CargoController {
	
	@Autowired
	private CargoService cargoService;
	@Autowired
	private DepartamentoService departamentoService;
	

	@GetMapping("/cadastrar")
	public String cadastrar(Cargo cargo) {
		return "/cargo/cadastroCargo";
	}
	
	@GetMapping ("/listar")
	public String listar(ModelMap model) {
		model.addAttribute("cargos", cargoService.buscarTodos());
		return "/cargo/listaCargo";
	}
	
	@GetMapping ("/relatorio")
	public String relatorio(ModelMap model) {
		model.addAttribute("cargos", cargoService.buscarTodos());
		return "/cargo/relatorioCargo";
	}
	
	@PostMapping ("/salvar")
	public String salvar(Cargo cargo, RedirectAttributes attr) {
		cargoService.salvar(cargo);
		attr.addFlashAttribute("success", "Cargo inserido com sucesso!");
		return "redirect:/cargos/cadastrar";
	}
	
	@GetMapping ("/editar/{id}")
	public String preEditar(@PathVariable ("id") Long id, ModelMap model) {
		model.addAttribute ("cargo", cargoService.buscarPorId(id));
		return "cargo/cadastroCargo";
	}
	
	@PostMapping ("/editar")
	public String editar(Cargo cargo, RedirectAttributes attr) {
		cargoService.editar(cargo);
		attr.addFlashAttribute("success", "Departamento editado com sucesso!");
		return "redirect:/cargos/cadastrar";
	}
	
	@GetMapping ("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attr) {
		if (cargoService.cargoTemFuncionarios(id)) {
			attr.addFlashAttribute("fail", "Cargo não removido! possui possui funcionário(s) vinculados!");
		} else {
			cargoService.excluir(id);
			attr.addFlashAttribute("success", "Cargo excluído com sucesso!");
		};
		
		return "redirect:/cargos/listar";
	} 
	
	@ModelAttribute ("departamentos")
	public List<Departamento> listaDepartamentos() {
		return departamentoService.buscarTodos();
	}
	
}
