
package com.systecos.boot.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	@GetMapping("/")
	public String home() {
		return "home";
	}
	
	@GetMapping("layout")
	public String layout() {
		return "layout";
	}
	
	@GetMapping("cadastro")
	public String cadastro() {
		return "cadastro";
	}
	
	@GetMapping("relatorio")
	public String relatorio() {
		return "relatorio";
	}
	
	@GetMapping({ "/login" })
	public String login() {
		return "login";
	}

}