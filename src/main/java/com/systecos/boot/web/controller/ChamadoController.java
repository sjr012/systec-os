package com.systecos.boot.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping ("/chamados")
public class ChamadoController {

	@GetMapping("/cadastrar")
	public String cadastrar() {
		return "/chamado/cadastroChamado";
	}
	
	@GetMapping ("/listar")
	public String listar() {
		return "/chamado/listaChamado";
	}
	
	@GetMapping ("/relatorio")
	public String relatorio() {
		return "/chamado/relatorioChamado";
	}
	
}
