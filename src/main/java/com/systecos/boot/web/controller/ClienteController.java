package com.systecos.boot.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping ("/clientes")
public class ClienteController {
	
	@GetMapping("/cadastrar")
	public String cadastrar() {
		return "/cliente/cadastroCliente";
	}
	
	@GetMapping ("/listar")
	public String listar() {
		return "/cliente/listaCliente";
	}
	
	@GetMapping ("/relatorio")
	public String relatorio() {
		return "/cliente/relatorioCliente";
	}
	
}
