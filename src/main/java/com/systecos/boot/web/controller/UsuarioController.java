
package com.systecos.boot.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.systecos.boot.domain.Cargo;
import com.systecos.boot.domain.UF;
import com.systecos.boot.domain.Usuario;
import com.systecos.boot.service.CargoService;
import com.systecos.boot.service.UsuarioService;

@Controller
@RequestMapping ("/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private CargoService cargoService;	
	
	@GetMapping("/cadastrar")
	public String cadastrar(Usuario usuario) {
		return "/usuario/cadastroUsuario";
	}
	
	@GetMapping ("/listar")
	public String listar() {
		return "/usuario/listaUsuario";
	}
	
	@GetMapping ("/relatorio")
	public String relatorio() {
		return "/usuario/relatorioUsuario";
	}
	
	@PostMapping ("/salvar")
	public String salvar(Usuario usuario, RedirectAttributes attr) {
		usuarioService.salvar(usuario);
		attr.addFlashAttribute("success", "Usuário inserido com sucesso!");
		return "redirect:/usuarios/cadastrar"; 
	}

	@ModelAttribute("cargos")
	public List<Cargo> getCargos() {
		return cargoService.buscarTodos();
	}
	
	@ModelAttribute("ufs")
	public UF[] getUfs() {
		return UF.values();
	}
	
}
