package com.systecos.boot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.systecos.boot.dao.UsuarioDao;
import com.systecos.boot.domain.Usuario;

@Service
@Transactional (readOnly = true)
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioDao dao;
	
	@Override
	@Transactional(readOnly = false)
	public void salvar(Usuario usuario) {
		dao.save(usuario);
		
	}

	@Override
	@Transactional(readOnly = false)
	public void editar(Usuario usuario) {
		dao.update(usuario);
		
	}

	@Override
	@Transactional(readOnly = false)
	public void excluir(Long id) {
		dao.delete(id);
		
	}

	@Override
	public Usuario buscarPorId(Long id) {
		
		return dao.findById(id);
	}

	@Override
	public List<Usuario> buscarTodos() {
		
		return dao.findAll();
	}
	
}
