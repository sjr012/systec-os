package com.systecos.boot.service;

import java.util.List;

import com.systecos.boot.domain.Usuario;

public interface UsuarioService {

	void salvar (Usuario usuario);
	
	void editar (Usuario usuario);
	
	void excluir (Long id);
	
	Usuario buscarPorId(Long id);
	
	List<Usuario> buscarTodos();
	
}
