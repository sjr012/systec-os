package com.systecos.boot.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@SuppressWarnings("serial")
@Entity
@Table (name = "CHAMADOS")
public class Chamado extends AbstractEntity<Long> {

	@Column (nullable = false, unique = true)
	private String nome_cliente;
	
	@DateTimeFormat(iso = ISO.DATE)
	@Column (name = "data_chamado", nullable = false, columnDefinition = "DATE")
	private Date data_chamado;
	
	@Column (nullable = false)
	private String status;
	
	@Column
	private String motivo;
	
	@Column
	private String solucao;
	
}
