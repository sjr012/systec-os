package com.systecos.boot.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;


@SuppressWarnings("serial")
@Entity
@Table (name = "FUNCIONARIOS")
public class Funcionario extends AbstractEntity<Long> {

	@Column (nullable = false, unique = true)
	private String nome;
	
	@NumberFormat(style = Style.CURRENCY, pattern = "#,##0.00")
	@Column (nullable = false, columnDefinition = "DECIMAL(7,2) DEFAULT #,##0.00")
	private BigDecimal salario;
	
	@NotNull
	@DateTimeFormat(iso = ISO.DATE)
	@Column (name = "data_entrada", nullable = false, columnDefinition = "DATE")
	private Date dataEntrada;
	
	@DateTimeFormat(iso = ISO.DATE)
	@Column (name = "data_saida", columnDefinition = "DATE")
	private Date dataSaida;
	
	@OneToOne (cascade = CascadeType.ALL)
	@JoinColumn (name = "endereco_id_fk")
	private Endereco endereco;
	
	@ManyToOne
	@JoinColumn (name = "cargo_id_fk")
	private Cargo cargo;
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getSalario() {
		return salario;
	}

	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}

	public Date getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(@NotNull Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public Date getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(Date dataSaida) {
		this.dataSaida = dataSaida;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
}
