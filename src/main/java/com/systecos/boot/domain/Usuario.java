package com.systecos.boot.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table (name = "USUARIOS")
public class Usuario extends AbstractEntity<Long> {
	
	@Column
	private String nome;
	
	@OneToOne (cascade = CascadeType.ALL)
	@JoinColumn (name = "endereco_id_fk")
	private Endereco endereco;
	
	@Column
	private String telefone;
	
	@Column
	private String tipo;
	
	@Id
	@Column (nullable = false, unique = true)
	private String login;
	
	@Column
	private String senha;
	
}
